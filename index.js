const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const http = require('http');
const socketIO = require('socket.io');
const configsocket = require('./src/sockets/sockets')

const app = express();

const httpServer = http.createServer(app);
const io = socketIO(httpServer);



const port = process.env.PORT || 3000;


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors({ origin: true, credentials: true }));

app.use(require('./src/routes/index.routes'));



httpServer.listen(port, () => {
    console.log('Servidor iniciado en el puerto', 3000);
});

io.on('connection', cliente => {

    //coenctar cliente 
    configsocket.conectarcliente(cliente);
    configsocket.login(cliente);
    console.log('Cliente conectado',cliente.id);
    configsocket.mensaje(cliente,io);
    configsocket.desconectar(cliente);
    
});

