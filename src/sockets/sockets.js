const crud = require('../models/listausuarios.model');



let sockets = {};

sockets.desconectar = (cliente) => {

    cliente.on('disconnect', () => {
        crud.deleteUser(cliente.id);
        console.log('cliente desconectado');
    });
};

sockets.mensaje = (cliente, io) => {
    cliente.on('mensaje', (payload) => {
        console.log('Mensaje Recibido', payload);
        io.emit('mensaje-nuevo', payload);
    });

}

sockets.login = (cliente) => {
    cliente.on('config user', ( payload , cb) => {
        crud.actualizar(cliente.id,payload.nombre);
        cb({
            ok: true,
            message: `user ${payload.nombre} listo`
        })
    });
}

sockets.conectarcliente = (cliente) => {

    let usuario = {
        id: cliente.id,
        nombre: 'sin-nombre',
        sala: 'sin-sala'
    };

    crud.agregar(usuario);
};



module.exports = sockets;