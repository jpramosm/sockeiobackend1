let crud = {};



let usuarios  = [];

crud.agregar = (user) => {
    usuarios.push(user);
    console.log(usuarios);
    return user;
};

crud.actualizar = (id, nombre) => {
    for (const user of usuarios) {
        if (user.id === id) {
            user.nombre = nombre;
            break;
        }
    };
    console.log('Participante actualizado',usuarios);
};

crud.getlista = () => {
    return usuarios;
};

crud.getUser = (id) => {
    return usuarios.find(item => item.id === id)
};

crud.getroom = (sala) => {
    return usuarios.filter(user => user.sala = sala);
};

crud.deleteUser = (id) => {
    let eliminado = usuarios.findIndex(user => user.id == id);
    usuarios.splice(eliminado,1);
    console.log('participante desconectado',usuarios);
    return eliminado;
}

module.exports = crud


